package com.adriang.relojes.base;

import java.time.LocalDate;

public abstract class Reloj {

    private String marca;
    private String correa;
    private String material;
    private LocalDate fechaFabricacion;


    public Reloj(){

    }

    public Reloj(String marca, String correa, String material, LocalDate fechaFabricacion){
        this.marca=marca;
        this.correa=correa;
        this.material=material;
        this.fechaFabricacion=fechaFabricacion;

    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public String getCorrea() {
        return correa;
    }

    public void setCorrea(String correa) {
        this.correa = correa;
    }

    public String getMaterial() {
        return material;
    }

    public void setMaterial(String material) {
        this.material = material;
    }

    public LocalDate getFechaFabricacion() {
        return fechaFabricacion;
    }

    public void setFechaFabricacion(LocalDate fechaFabricacion) {
        this.fechaFabricacion = fechaFabricacion;
    }
}
