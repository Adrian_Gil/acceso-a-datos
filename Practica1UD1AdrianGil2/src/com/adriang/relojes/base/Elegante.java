package com.adriang.relojes.base;

import java.time.LocalDate;

public class Elegante extends Reloj{
    private int cantidadEsferas;

    public Elegante(){ super();}

    public Elegante(String marca, String correa, String material, LocalDate fechaFabricacion, int numeroEsferas) {
        super(marca, correa, material, fechaFabricacion);
        this.cantidadEsferas = cantidadEsferas;
    }

    public int getCantidadEsferas() {
        return cantidadEsferas;
    }

    public void setCantidadEsferas(int cantidadEsferas) {
        this.cantidadEsferas = cantidadEsferas;
    }

    @Override
    public String toString() {
        return "Elegante{"
                +"marca= "+getMarca()
                +"correa= "+getCorrea()
                +"material= "+getMaterial()
                + "fecha_fabricacion= "+getFechaFabricacion()+
                '}';
    }
}
