package com.adriang.relojes;

import com.adriang.relojes.gui.RelojControlador;
import com.adriang.relojes.gui.RelojModelo;
import com.adriang.relojes.gui.Ventana;

import java.io.IOException;

public class Principal {
    public static void main(String[] args) throws IOException {
        Ventana vista= new Ventana();
        RelojModelo modelo= new RelojModelo();
        RelojControlador controlador= new RelojControlador(vista,modelo);
    }
}
