package gui;

import base.enums.ModeloRelojes;
import base.enums.TiposFabricantes;
import com.github.lgooddatepicker.components.DatePicker;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;

public class Vista extends JFrame {
    private final static String TITULOFRAME="Aplicación Varias Tablas";
    private JTabbedPane tabbedPane;
    private JPanel panel1;
    private JPanel JPanelReloj;
    private JPanel JPanelComprador;
    private JPanel JPanelFabricante;

    //RELOJES
    JTextField txtModelo;
    JComboBox comboComprador;
    JComboBox comboFabricante;
    JComboBox comboModelo;
    DatePicker fecha;
    JTextField txtCodigoBarras;
    JTextField txtPrecioReloj;
    JTable relojesTabla;
    JButton anadirReloj;
    JButton modificarReloj;
    JButton eliminarReloj;

    //COMPRADORES
    JTextField txtNombre;
    JTextField txtApellidos;
    DatePicker fechaNacimiento;
    JTextField txtPais;
    JTable compradoresTabla;
    JButton eliminarComprador;
    JButton anadirComprador;
    JButton modificarComprador;

    //FABRICANTES
    JTextField txtNombreFabricante;
    JTextField txtEmail;
    JTextField txtTelefono;
    JComboBox comboTipoFabricante;
    JTextField txtWeb;
    JTable fabricantesTabla;
    JButton eliminarFabricante;
    JButton anadirFabricante;
    JButton modificarFabricante;


    //BUSQUEDA
    JLabel etiquetaEstado;

    //DEFAULT TABLE MODEL
    DefaultTableModel dtmFabricantes;
    DefaultTableModel dtmCompradores;
    DefaultTableModel dtmRelojes;

    //BARRA MENU
    JMenuItem itemOpciones;
    JMenuItem itemDesconectar;
    JMenuItem itemSalir;

    //CUADRO DIALOGO
    OptionDialog optionDialog;
    JDialog adminPasswordDialog;
    JButton btnValidate;
    JPasswordField adminPassword;

    public Vista() {
        super(TITULOFRAME);
        initFrame();
    }

    public void initFrame() {
        this.setContentPane(panel1);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.pack();
        this.setVisible(true);
        this.setSize(new Dimension(this.getWidth()+200,this.getHeight()+100));
        this.setLocationRelativeTo(this);
        //creo cuadro de dialogo
        optionDialog=new OptionDialog(this);
        setMenu();
        setAdminDialog();
        setEnumComboBox();
        setTableModels();
    }

    private void setTableModels() {
        this.dtmRelojes =new DefaultTableModel();
        this.relojesTabla.setModel(dtmRelojes);
        this.dtmCompradores =new DefaultTableModel();
        this.compradoresTabla.setModel(dtmCompradores);
        this.dtmFabricantes =new DefaultTableModel();
        this.fabricantesTabla.setModel(dtmFabricantes);
    }

    private void setMenu() {
        JMenuBar mbBar = new JMenuBar();
        JMenu menu = new JMenu("Archivo");
        //por cada item que tenga funcionalidad tiene un ActionCommand
        itemOpciones= new JMenuItem("Opciones");
        itemOpciones.setActionCommand("Opciones");
        itemDesconectar= new JMenuItem("Desconectar");
        itemDesconectar.setActionCommand("Desconectar");
        itemSalir=new JMenuItem("Salir");
        itemSalir.setActionCommand("Salir");
        menu.add(itemOpciones);
        menu.add(itemDesconectar);
        menu.add(itemSalir);
        mbBar.add(menu);
        //centrar en horizontal
        mbBar.add(Box.createHorizontalGlue());
        this.setJMenuBar(mbBar);
    }

    private void setEnumComboBox() {
        for (TiposFabricantes constant: TiposFabricantes.values()) {
            comboTipoFabricante.addItem(constant.getValor());
        }
        comboTipoFabricante.setSelectedIndex(-1);
        for (ModeloRelojes constant: ModeloRelojes.values()) {
            comboModelo.addItem(constant.getValor());
        }
        comboModelo.setSelectedIndex(-1);
    }

    private void setAdminDialog() {
        btnValidate= new JButton("Validar");
        btnValidate.setActionCommand("abrirOpciones");
        adminPassword= new JPasswordField();
        adminPassword.setPreferredSize(new Dimension(100,26));
        Object[] options = new Object[] {adminPassword,btnValidate};
        JOptionPane jop =new JOptionPane("Introduce la contraseña",
                JOptionPane.WARNING_MESSAGE,JOptionPane.YES_NO_OPTION,null,options);
        adminPasswordDialog=new JDialog(this,"Opciones",true);
        adminPasswordDialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
        adminPasswordDialog.setContentPane(jop);
        adminPasswordDialog.pack();
        adminPasswordDialog.setLocationRelativeTo(this);
    }

}
