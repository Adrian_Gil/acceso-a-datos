package gui;

import util.Util;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;
import java.awt.event.*;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.Vector;

/**
 * Created by DAM on 13/12/2021.
 */
public class Controlador implements ActionListener, ItemListener, ListSelectionListener, WindowListener {
    private Modelo modelo;
    private Vista vista;
    boolean refrescar;

    public Controlador(Vista vista, Modelo modelo) {
        this.modelo = modelo;
        this.vista = vista;
        modelo.conectar();
        setOptions();
        addActionListeners(this);
        addItemListeners(this);
        addWindowListeners(this);
        refrescarTodo();
    }

    private void refrescarTodo() {
        refrescarCompradores();
        refrescarFabricantes();
        refrescarRelojes();
        refrescar = false;
    }

    private void addActionListeners(ActionListener listener) {
        vista.anadirReloj.addActionListener(listener);
        vista.anadirReloj.setActionCommand("anadirReloj");
        vista.anadirComprador.addActionListener(listener);
        vista.anadirComprador.setActionCommand("anadirComprador");
        vista.anadirFabricante.addActionListener(listener);
        vista.anadirFabricante.setActionCommand("anadirFabricante");
        vista.eliminarReloj.addActionListener(listener);
        vista.eliminarReloj.setActionCommand("eliminarReloj");
        vista.eliminarComprador.addActionListener(listener);
        vista.eliminarComprador.setActionCommand("eliminarComprador");
        vista.eliminarFabricante.addActionListener(listener);
        vista.eliminarFabricante.setActionCommand("eliminarFabricante");
        vista.modificarReloj.addActionListener(listener);
        vista.modificarReloj.setActionCommand("modificarReloj");
        vista.modificarComprador.addActionListener(listener);
        vista.modificarComprador.setActionCommand("modificarComprador");
        vista.modificarFabricante.addActionListener(listener);
        vista.modificarFabricante.setActionCommand("modificarFabricante");
        vista.optionDialog.btnOpcionesGuardar.addActionListener(listener);
        vista.itemOpciones.addActionListener(listener);
        vista.itemSalir.addActionListener(listener);
        vista.btnValidate.addActionListener(listener);
    }

    private void addWindowListeners(WindowListener listener) {
        vista.addWindowListener(listener);
    }

    private void addItemListeners(Controlador controlador) {
    }

    /**
     * Muestra los atributos de un objeto seleccionado y los borra una vez se deselecciona
     *
     * @param e Evento producido en una lista
     */

    @Override
    public void valueChanged(ListSelectionEvent e) {
        if (e.getValueIsAdjusting()
                && !((ListSelectionModel) e.getSource()).isSelectionEmpty()) {
            if (e.getSource().equals(vista.fabricantesTabla.getSelectionModel())) {
                int row = vista.fabricantesTabla.getSelectedRow();
                vista.txtNombreFabricante.setText(String.valueOf(vista.fabricantesTabla.getValueAt(row, 1)));
                vista.txtEmail.setText(String.valueOf(vista.fabricantesTabla.getValueAt(row, 2)));
                vista.txtTelefono.setText(String.valueOf(vista.fabricantesTabla.getValueAt(row, 3)));
                vista.comboTipoFabricante.setSelectedItem(String.valueOf(vista.fabricantesTabla.getValueAt(row, 4)));
                vista.txtWeb.setText(String.valueOf(vista.fabricantesTabla.getValueAt(row, 5)));
            } else if (e.getSource().equals(vista.compradoresTabla.getSelectionModel())) {
                int row = vista.compradoresTabla.getSelectedRow();
                vista.txtNombre.setText(String.valueOf(vista.compradoresTabla.getValueAt(row, 1)));
                vista.txtApellidos.setText(String.valueOf(vista.compradoresTabla.getValueAt(row, 2)));
                vista.fechaNacimiento.setDate((Date.valueOf(String.valueOf(vista.compradoresTabla.getValueAt(row, 3)))).toLocalDate());
                vista.txtPais.setText(String.valueOf(vista.compradoresTabla.getValueAt(row, 4)));
            } else if (e.getSource().equals(vista.relojesTabla.getSelectionModel())) {
                int row = vista.relojesTabla.getSelectedRow();
                vista.txtModelo.setText(String.valueOf(vista.relojesTabla.getValueAt(row, 1)));
                vista.comboComprador.setSelectedItem(String.valueOf(vista.relojesTabla.getValueAt(row, 5)));
                vista.comboFabricante.setSelectedItem(String.valueOf(vista.relojesTabla.getValueAt(row, 3)));
                vista.comboModelo.setSelectedItem(String.valueOf(vista.relojesTabla.getValueAt(row, 4)));
                vista.fecha.setDate((Date.valueOf(String.valueOf(vista.relojesTabla.getValueAt(row, 7)))).toLocalDate());
                vista.txtCodigoBarras.setText(String.valueOf(vista.relojesTabla.getValueAt(row, 2)));
                vista.txtPrecioReloj.setText(String.valueOf(vista.relojesTabla.getValueAt(row, 6)));
            } else if (e.getValueIsAdjusting()
                    && ((ListSelectionModel) e.getSource()).isSelectionEmpty() && !refrescar) {
                if (e.getSource().equals(vista.fabricantesTabla.getSelectionModel())) {
                    borrarCamposFabricantes();
                } else if (e.getSource().equals(vista.compradoresTabla.getSelectionModel())) {
                    borrarCamposCompradores();
                } else if (e.getSource().equals(vista.relojesTabla.getSelectionModel())) {
                    borrarCamposRelojes();
                }
            }
        }
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        String command = e.getActionCommand();
        switch (command) {
            case "Opciones":
                vista.adminPasswordDialog.setVisible(true);
                break;
            case "Desconectar":
                modelo.desconectar();
                break;
            case "Salir":
                System.exit(0);
                break;
            case "abrirOpciones":
                if (String.valueOf(vista.adminPassword.getPassword()).equals(modelo.getAdminPassword())) {
                    vista.adminPassword.setText("");
                    vista.adminPasswordDialog.dispose();
                    vista.optionDialog.setVisible(true);
                } else {
                    Util.showErrorAlert("La contraseña introducida no es correcta");
                }
                break;
            case "guardarOpciones":
                modelo.setPropValues(vista.optionDialog.tfIP.getText(),
                        vista.optionDialog.tfUser.getText(),
                        String.valueOf(vista.optionDialog.pfPass.getPassword()),
                        String.valueOf(vista.optionDialog.pfAdmin.getPassword()));
                vista.optionDialog.dispose();
                vista.dispose();
                new Controlador(new Vista(), new Modelo());
                break;
            case "anadirReloj":
                try {
                    if (comprobarRelojVacio()) {
                        Util.showErrorAlert("Rellena todos los campos");
                        vista.relojesTabla.clearSelection();
                    } else if (modelo.relojCodigoBarrasYaExiste(vista.txtCodigoBarras.getText())) {
                        Util.showErrorAlert("Ese codigo de barras ya existe");
                        vista.relojesTabla.clearSelection();
                    } else {
                        modelo.insertarReloj(
                                vista.txtModelo.getText(),
                                vista.txtCodigoBarras.getText(),
                                String.valueOf(vista.comboFabricante.getSelectedItem()),
                                String.valueOf(vista.comboModelo.getSelectedItem()),
                                String.valueOf(vista.comboComprador.getSelectedItem()),
                                Float.parseFloat(vista.txtPrecioReloj.getText()),
                                vista.fecha.getDate());
                    }
                } catch (NumberFormatException nfe) {
                    Util.showErrorAlert("Introduce numeros en los campos que lo requieran");
                    vista.relojesTabla.clearSelection();
                }
                borrarCamposRelojes();
                refrescarRelojes();
                break;
            case "modificarReloj":
                try {
                    if (comprobarRelojVacio()) {
                        Util.showErrorAlert("Rellena todos los campos");
                        vista.relojesTabla.clearSelection();
                    } else {
                        modelo.modificarReloj(
                                vista.txtModelo.getText(),
                                vista.txtCodigoBarras.getText(),
                                String.valueOf(vista.comboFabricante.getSelectedItem()),
                                String.valueOf(vista.comboModelo.getSelectedItem()),
                                String.valueOf(vista.comboComprador.getSelectedItem()),
                                Float.parseFloat(vista.txtPrecioReloj.getText()),
                                vista.fecha.getDate(),
                                Integer.parseInt((String) vista.relojesTabla.getValueAt(vista.relojesTabla.getSelectedRow(), 0))
                        );
                    }
                } catch (NumberFormatException nfe) {
                    Util.showErrorAlert("Introduce numeros en los campos que lo requieran");
                    vista.relojesTabla.clearSelection();
                }
                borrarCamposRelojes();
                refrescarRelojes();
                break;
            case "eliminarReloj":
                modelo.borrarReloj(Integer.parseInt((String) vista.relojesTabla.getValueAt(vista.relojesTabla.getSelectedRow(), 0)));
                borrarCamposRelojes();
                refrescarRelojes();
                break;
            case "anadirComprador": {
                try {
                    if (comprobarCompradorVacio()) {
                        Util.showErrorAlert("Rellena todos los campos");
                        vista.compradoresTabla.clearSelection();
                    } else if (modelo.compradorNombreYaExiste(vista.txtNombre.getText(),
                            vista.txtApellidos.getText())) {
                        Util.showErrorAlert("Ese nombre ya existe.\nIntroduce un autor diferente");
                        vista.compradoresTabla.clearSelection();
                    } else {
                        modelo.insertarComprador(vista.txtNombre.getText(),
                                vista.txtApellidos.getText(),
                                vista.fechaNacimiento.getDate(),
                                vista.txtPais.getText());
                        refrescarCompradores();
                    }
                } catch (NumberFormatException nfe) {
                    Util.showErrorAlert("Introduce números en los campos que lo requieren");
                    vista.compradoresTabla.clearSelection();
                }
                borrarCamposCompradores();
            }
            break;
            case "modificarComprador": {
                try {
                    if (comprobarCompradorVacio()) {
                        Util.showErrorAlert("Rellena todos los campos");
                        vista.compradoresTabla.clearSelection();
                    } else {
                        modelo.modificarComprador(vista.txtNombre.getText(), vista.txtApellidos.getText(),
                                vista.fechaNacimiento.getDate(), vista.txtPais.getText(),
                                Integer.parseInt((String) vista.compradoresTabla.getValueAt(vista.compradoresTabla.getSelectedRow(), 0)));
                        refrescarCompradores();
                    }
                } catch (NumberFormatException nfe) {
                    Util.showErrorAlert("Introduce números en los campos que lo requieren");
                    vista.compradoresTabla.clearSelection();
                }
                borrarCamposCompradores();
            }
            break;
            case "eliminarComprador":
                modelo.borrarComprador(Integer.parseInt((String) vista.compradoresTabla.getValueAt(vista.compradoresTabla.getSelectedRow(), 0)));
                borrarCamposCompradores();
                refrescarCompradores();
                break;
            case "anadirFabricante": {
                try {
                    if (comprobarFabricantesVacia()) {
                        Util.showErrorAlert("Rellena todos los campos");
                        vista.fabricantesTabla.clearSelection();
                    } else if (modelo.fabricanteNombreYaExiste(vista.txtNombreFabricante.getText())) {
                        Util.showErrorAlert("Ese nombre ya existe.\nIntroduce una editorial diferente.");
                        vista.fabricantesTabla.clearSelection();
                    } else {
                        modelo.insertarFabricante(vista.txtNombreFabricante.getText(), vista.txtEmail.getText(),
                                vista.txtTelefono.getText(),
                                (String) vista.comboTipoFabricante.getSelectedItem(),
                                vista.txtWeb.getText());
                        refrescarFabricantes();
                    }
                } catch (NumberFormatException nfe) {
                    Util.showErrorAlert("Introduce números en los campos que lo requieren");
                    vista.fabricantesTabla.clearSelection();
                }
                borrarCamposFabricantes();
            }
            break;
            case "modificarFabricante": {
                try {
                    if (comprobarFabricantesVacia()) {
                        Util.showErrorAlert("Rellena todos los campos");
                        vista.fabricantesTabla.clearSelection();
                    } else {
                        modelo.modificarFabricantes(vista.txtNombreFabricante.getText(), vista.txtEmail.getText(), vista.txtTelefono.getText(),
                                String.valueOf(vista.comboTipoFabricante.getSelectedItem()), vista.txtWeb.getText(),
                                Integer.parseInt((String) vista.fabricantesTabla.getValueAt(vista.fabricantesTabla.getSelectedRow(), 0)));
                        refrescarFabricantes();
                    }
                } catch (NumberFormatException nfe) {
                    Util.showErrorAlert("Introduce números en los campos que lo requieren");
                    vista.fabricantesTabla.clearSelection();
                }
                borrarCamposFabricantes();
            }
            break;
            case "eliminarFabricante":
                modelo.borrarFabricante(Integer.parseInt((String) vista.fabricantesTabla.getValueAt(vista.fabricantesTabla.getSelectedRow(), 0)));
                borrarCamposFabricantes();
                refrescarFabricantes();
                break;
        }
    }

    private void refrescarFabricantes() {
        try {
            vista.fabricantesTabla.setModel(construirTableModelFabricantes(modelo.consultarFabricante()));
            vista.comboFabricante.removeAllItems();
            for (int i = 0; i < vista.dtmFabricantes.getRowCount(); i++) {
                vista.comboFabricante.addItem(vista.dtmFabricantes.getValueAt(i, 0) + " - " +
                        vista.dtmFabricantes.getValueAt(i, 1));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private DefaultTableModel construirTableModelFabricantes(ResultSet rs)
            throws SQLException {
        ResultSetMetaData metaData = rs.getMetaData();
        // names of columns
        Vector<String> columnNames = new Vector<>();
        int columnCount = metaData.getColumnCount();
        for (int column = 1; column <= columnCount; column++) {
            columnNames.add(metaData.getColumnName(column));
        }
        // data of the table
        Vector<Vector<Object>> data = new Vector<>();
        setDataVector(rs, columnCount, data);
        vista.dtmFabricantes.setDataVector(data, columnNames);
        return vista.dtmFabricantes;
    }

    private void refrescarCompradores() {
        try {
            vista.compradoresTabla.setModel(construirTableModeloComprador(modelo.consultarComprador()));
            vista.comboComprador.removeAllItems();
            for (int i = 0; i < vista.dtmCompradores.getRowCount(); i++) {
                vista.comboComprador.addItem(vista.dtmCompradores.getValueAt(i, 0) + " - " +
                        vista.dtmCompradores.getValueAt(i, 2) + ", " + vista.dtmCompradores.getValueAt(i, 1));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private DefaultTableModel construirTableModeloComprador(ResultSet rs)
            throws SQLException {
        ResultSetMetaData metaData = rs.getMetaData();
        // names of columns
        Vector<String> columnNames = new Vector<>();
        int columnCount = metaData.getColumnCount();
        for (int column = 1; column <= columnCount; column++) {
            columnNames.add(metaData.getColumnName(column));
        }
        // data of the table
        Vector<Vector<Object>> data = new Vector<>();
        setDataVector(rs, columnCount, data);
        vista.dtmCompradores.setDataVector(data, columnNames);
        return vista.dtmCompradores;
    }

    /**
     * Actualiza los libros que se ven en la lista y los comboboxes
     */
    private void refrescarRelojes() {
        try {
            vista.relojesTabla.setModel(construirTableModelRelojes(modelo.consultarRelojes()));
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private DefaultTableModel construirTableModelRelojes(ResultSet rs)
            throws SQLException {
        ResultSetMetaData metaData = rs.getMetaData();
        // names of columns
        Vector<String> columnNames = new Vector<>();
        int columnCount = metaData.getColumnCount();
        for (int column = 1; column <= columnCount; column++) {
            columnNames.add(metaData.getColumnName(column));
        }
        // data of the table
        Vector<Vector<Object>> data = new Vector<>();
        setDataVector(rs, columnCount, data);
        vista.dtmRelojes.setDataVector(data, columnNames);
        return vista.dtmRelojes;

    }

    private void setDataVector(ResultSet rs, int columnCount, Vector<Vector<Object>> data) throws SQLException {
        while (rs.next()) {
            Vector<Object> vector = new Vector<>();
            for (int columnIndex = 1; columnIndex <= columnCount; columnIndex++) {
                vector.add(rs.getObject(columnIndex));
            }
            data.add(vector);
        }
    }

    private void setOptions() {
    }

    private void borrarCamposRelojes() {
        vista.comboFabricante.setSelectedIndex(-1);
        vista.comboComprador.setSelectedIndex(-1);
        vista.txtModelo.setText("");
        vista.txtCodigoBarras.setText("");
        vista.comboModelo.setSelectedIndex(-1);
        vista.txtPrecioReloj.setText("");
        vista.fecha.setText("");
    }

    private void borrarCamposCompradores() {
        vista.txtNombre.setText("");
        vista.txtApellidos.setText("");
        vista.txtPais.setText("");
        vista.fechaNacimiento.setText("");
    }

    private void borrarCamposFabricantes() {
        vista.txtNombreFabricante.setText("");
        vista.txtEmail.setText("");
        vista.txtTelefono.setText("");
        vista.comboTipoFabricante.setSelectedIndex(-1);
        vista.txtWeb.setText("");
    }

    private boolean comprobarRelojVacio() {
        return vista.txtModelo.getText().isEmpty() ||
                vista.txtPrecioReloj.getText().isEmpty() ||
                vista.txtCodigoBarras.getText().isEmpty() ||
                vista.comboModelo.getSelectedIndex() == -1 ||
                vista.comboComprador.getSelectedIndex() == -1 ||
                vista.comboFabricante.getSelectedIndex() == -1 ||
                vista.fecha.getText().isEmpty();
    }

    private boolean comprobarCompradorVacio() {
        return vista.txtApellidos.getText().isEmpty() ||
                vista.txtNombre.getText().isEmpty() ||
                vista.txtPais.getText().isEmpty() ||
                vista.fechaNacimiento.getText().isEmpty();
    }

    private boolean comprobarFabricantesVacia() {
        return vista.txtNombreFabricante.getText().isEmpty() ||
                vista.txtEmail.getText().isEmpty() ||
                vista.txtTelefono.getText().isEmpty() ||
                vista.comboTipoFabricante.getSelectedIndex() == -1 ||
                vista.txtWeb.getText().isEmpty();
    }

    @Override
    public void itemStateChanged(ItemEvent e) {

    }

    @Override
    public void windowOpened(WindowEvent e) {

    }

    @Override
    public void windowClosing(WindowEvent e) {

    }

    @Override
    public void windowClosed(WindowEvent e) {

    }

    @Override
    public void windowIconified(WindowEvent e) {

    }

    @Override
    public void windowDeiconified(WindowEvent e) {

    }

    @Override
    public void windowActivated(WindowEvent e) {

    }

    @Override
    public void windowDeactivated(WindowEvent e) {

    }
}
