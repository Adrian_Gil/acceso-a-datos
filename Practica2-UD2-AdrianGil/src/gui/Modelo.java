package gui;

import java.io.*;
import java.sql.*;
import java.time.LocalDate;
import java.util.Properties;

/**
 * Created by DAM on 13/12/2021.
 */
public class Modelo {
    private String ip;
    private String user;
    private String password;
    private String adminPassword;

    public String getIp() {
        return ip;
    }

    public String getUser() {
        return user;
    }

    public String getPassword() {
        return password;
    }

    public String getAdminPassword() {
        return adminPassword;
    }

    public Modelo() {
        getPropValues();
    }

    private Connection conexion;

    void conectar() {
        try {
            conexion = DriverManager.getConnection("jdbc:mysql://"
                    + ip + ":3306/relojeria", user, password);
        } catch (SQLException e) {
            try {
                conexion = DriverManager.getConnection("jdbc:mysql://"
                        + ip + ":3306/", user, password);
                PreparedStatement statement = null;

                String code = leerFichero();
                String[] query = code.split("--");
                for (String aQuery : query) {
                    statement = conexion.prepareStatement(aQuery);
                    statement.executeUpdate();
                }
                assert statement != null;
                statement.close();
            } catch (SQLException | IOException e1) {
                e1.printStackTrace();
            }
            e.printStackTrace();
        }
    }

    private String leerFichero() throws IOException {
        //basedatos_java no tiene delimitador
        //StringBuilder es dinamica
        try (BufferedReader reader = new BufferedReader(new FileReader("basedatos_java.sql"))) {
            String linea;
            StringBuilder stringBuilder = new StringBuilder();
            while ((linea = reader.readLine()) != null) {
                stringBuilder.append(linea);
                stringBuilder.append(" ");
            }
            return stringBuilder.toString();
        }
    }

    void desconectar() {
        try {
            conexion.close();
            conexion = null;
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    void insertarComprador(String nombre, String apellidos, LocalDate fechaNacimiento, String pais) {
        String sentenciaSql = "INSERT INTO compradores (nombre, apellidos, fechanacimiento, pais)" +
                "VALUES (?,?,?,?)";

        PreparedStatement sentencia = null;
        try {
            sentencia = conexion.prepareStatement(sentenciaSql);
            sentencia.setString(1, nombre);
            sentencia.setString(2, apellidos);
            sentencia.setDate(3, Date.valueOf(fechaNacimiento));
            sentencia.setString(4, pais);
            sentencia.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (sentencia != null) {
                try {
                    sentencia.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }

    }

    void insertarFabricante(String fabricante, String email, String telefono, String tipoFabricante, String web) {
        String sentenciaSql = "INSERT INTO fabricantes (fabricante, email, telefono, tipofabricante, web) VALUES (?, ?, ?, ?, ?)";
        PreparedStatement sentencia = null;

        try {
            sentencia = conexion.prepareStatement(sentenciaSql);
            sentencia.setString(1, fabricante);
            sentencia.setString(2, email);
            sentencia.setString(3, telefono);
            sentencia.setString(4, tipoFabricante);
            sentencia.setString(5, web);
            sentencia.executeUpdate();
        } catch (SQLException sqle) {
            sqle.printStackTrace();
        } finally {
            if (sentencia != null)
                try {
                    sentencia.close();
                } catch (SQLException sqle) {
                    sqle.printStackTrace();
                }
        }
    }

    void insertarReloj(String marca, String codigoBarras, String fabricante, String modelo, String comprador,
                       float precio, LocalDate fechaLanzamiento) {
        String sentenciaSql = "INSERT INTO relojes (marca, codigoBarras, idfabricante, modelo, idcomprador, precio, fechalanzamiento) " +
                "VALUES (?, ?, ?, ?, ?, ?, ?)";
        PreparedStatement sentencia = null;

        int idfabricante = Integer.valueOf(fabricante.split(" ")[0]);
        int idcomprador = Integer.valueOf(comprador.split(" ")[0]);

        try {
            sentencia = conexion.prepareStatement(sentenciaSql);
            sentencia.setString(1, marca);
            sentencia.setString(2, codigoBarras);
            sentencia.setInt(3, idfabricante);
            sentencia.setString(4, modelo);
            sentencia.setInt(5, idcomprador);
            sentencia.setFloat(6, precio);
            sentencia.setDate(7, Date.valueOf(fechaLanzamiento));
            sentencia.executeUpdate();
        } catch (SQLException sqle) {
            sqle.printStackTrace();
        } finally {
            if (sentencia != null)
                try {
                    sentencia.close();
                } catch (SQLException sqle) {
                    sqle.printStackTrace();
                }
        }
    }

    void modificarComprador(String nombre, String apellidos, LocalDate fechanacimiento, String pais, int idcomprador) {
        String sentenciaSql = "UPDATE compradores SET nombre=?,apellidos=?,fechanacimiento=?,pais=?" +
                "WHERE idcomprador=?";
        PreparedStatement sentencia = null;
        try {
            sentencia = conexion.prepareStatement(sentenciaSql);
            sentencia.setString(1, nombre);
            sentencia.setString(2, apellidos);
            sentencia.setDate(3, Date.valueOf(fechanacimiento));
            sentencia.setString(4, pais);
            sentencia.setInt(5, idcomprador);
            sentencia.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (sentencia != null)
                try {
                    sentencia.close();
                } catch (SQLException sqle) {
                    sqle.printStackTrace();
                }
        }
    }

    void modificarFabricantes(String fabricante, String email, String telefono, String tipoFabricante, String web, int idfabricante) {

        String sentenciaSql = "UPDATE fabricantes SET fabricante = ?, email = ?, telefono = ?, tipofabricante = ?, web = ?" +
                "WHERE idfabricante = ?";
        PreparedStatement sentencia = null;

        try {
            sentencia = conexion.prepareStatement(sentenciaSql);
            sentencia.setString(1, fabricante);
            sentencia.setString(2, email);
            sentencia.setString(3, telefono);
            sentencia.setString(4, tipoFabricante);
            sentencia.setString(5, web);
            sentencia.setInt(6, idfabricante);
            sentencia.executeUpdate();
        } catch (SQLException sqle) {
            sqle.printStackTrace();
        } finally {
            if (sentencia != null)
                try {
                    sentencia.close();
                } catch (SQLException sqle) {
                    sqle.printStackTrace();
                }
        }
    }

    void modificarReloj(String marca, String codigoBarras, String fabricante, String modelo, String comprador,
                        float precio, LocalDate fechalanzamiento, int idreloj) {

        String sentenciaSql = "UPDATE relojes SET marca = ?, codigoBarras = ?, idfabricante = ?, modelo = ?," +
                "idcomprador = ?, precio = ?, fechalanzamiento = ? WHERE idreloj = ?";
        PreparedStatement sentencia = null;

        int idfabricante = Integer.valueOf(fabricante.split(" ")[0]);
        int idcomprador = Integer.valueOf(comprador.split(" ")[0]);

        try {
            sentencia = conexion.prepareStatement(sentenciaSql);
            sentencia.setString(1, marca);
            sentencia.setString(2, codigoBarras);
            sentencia.setInt(3, idfabricante);
            sentencia.setString(4, modelo);
            sentencia.setInt(5, idcomprador);
            sentencia.setFloat(6, precio);
            sentencia.setDate(7, Date.valueOf(fechalanzamiento));
            sentencia.setInt(8, idreloj);
            sentencia.executeUpdate();
        } catch (SQLException sqle) {
            sqle.printStackTrace();
        } finally {
            if (sentencia != null)
                try {
                    sentencia.close();
                } catch (SQLException sqle) {
                    sqle.printStackTrace();
                }
        }
    }

    void borrarFabricante(int idfabricante) {
        String sentenciaSql = "DELETE FROM fabricantes WHERE idfabricante=?";
        PreparedStatement sentencia = null;
        try {
            sentencia = conexion.prepareStatement(sentenciaSql);
            sentencia.setInt(1, idfabricante);
            sentencia.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (sentencia != null)
                try {
                    sentencia.close();
                } catch (SQLException sqle) {
                    sqle.printStackTrace();
                }
        }
    }

    void borrarComprador(int idcomprador) {
        String sentenciaSql = "DELETE FROM compradores WHERE idcomprador = ?";
        PreparedStatement sentencia = null;

        try {
            sentencia = conexion.prepareStatement(sentenciaSql);
            sentencia.setInt(1, idcomprador);
            sentencia.executeUpdate();
        } catch (SQLException sqle) {
            sqle.printStackTrace();
        } finally {
            if (sentencia != null)
                try {
                    sentencia.close();
                } catch (SQLException sqle) {
                    sqle.printStackTrace();
                }
        }
    }

    void borrarReloj(int idreloj) {
        String sentenciaSql = "DELETE FROM relojes WHERE idreloj = ?";
        PreparedStatement sentencia = null;

        try {
            sentencia = conexion.prepareStatement(sentenciaSql);
            sentencia.setInt(1, idreloj);
            sentencia.executeUpdate();
        } catch (SQLException sqle) {
            sqle.printStackTrace();
        } finally {
            if (sentencia != null)
                try {
                    sentencia.close();
                } catch (SQLException sqle) {
                    sqle.printStackTrace();
                }
        }
    }

    ResultSet consultarFabricante() throws SQLException {
        String sentenciaSql = "SELECT concat(idfabricante) AS 'ID', concat(fabricante) AS 'Nombre fabricante', " +
                "concat(email) AS 'email', concat(telefono) AS 'Teléfono',concat(tipofabricante) AS 'Tipo'," +
                "concat(web) AS 'Web' FROM fabricantes";
        PreparedStatement sentencia = null;
        ResultSet resultado = null;
        sentencia = conexion.prepareStatement(sentenciaSql);
        resultado = sentencia.executeQuery();
        return resultado;
    }

    ResultSet consultarComprador() throws SQLException {
        String sentenciaSql = "SELECT concat(idcomprador) AS 'ID', concat(nombre) AS 'Nombre', concat(apellidos) AS 'Apellidos', " +
                "concat(fechanacimiento) AS 'Fecha de nacimiento', concat(pais) AS 'País de origen' FROM compradores";
        PreparedStatement sentencia = null;
        ResultSet resultado = null;
        sentencia = conexion.prepareStatement(sentenciaSql);
        resultado = sentencia.executeQuery();
        return resultado;
    }

    ResultSet consultarRelojes() throws SQLException {
        String sentenciaSql = "SELECT concat(b.idreloj) AS 'ID', concat(b.marca) AS 'Marca', concat(b.codigoBarras) AS 'Codigo Barras', " +
                "concat(e.idfabricante, ' - ', e.fabricante) AS 'Fabricante', concat(b.modelo) AS 'Modelo', " +
                "concat(a.idcomprador, ' - ', a.apellidos, ', ', a.nombre) AS 'Comprador', " +
                "concat(b.precio) AS 'Precio', concat(b.fechalanzamiento) AS 'Fecha de publicación'" +
                " FROM relojes AS b " +
                "INNER JOIN fabricantes AS e ON e.idfabricante = b.idfabricante INNER JOIN " +
                "compradores AS a ON a.idcomprador = b.idcomprador";
        PreparedStatement sentencia = null;
        ResultSet resultado = null;
        sentencia = conexion.prepareStatement(sentenciaSql);
        resultado = sentencia.executeQuery();
        return resultado;
    }

    //usamos los datos del cuadro de dialogo
    private void getPropValues() {
        InputStream inputStream = null;
        try {
            Properties prop = new Properties();
            String propFileName = "config.properties";
            inputStream = new FileInputStream(propFileName);
            prop.load(inputStream);
            ip = prop.getProperty("ip");
            user = prop.getProperty("user");
            password = prop.getProperty("pass");
            adminPassword = prop.getProperty("admin");
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (inputStream != null)
                try {
                    inputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
        }
    }

    void setPropValues(String ip, String user, String pass, String adminPass) {
        try {
            Properties prop = new Properties();
            prop.setProperty("ip", ip);
            prop.setProperty("user", user);
            prop.setProperty("pass", pass);
            prop.setProperty("admin", adminPass);
            OutputStream out = new FileOutputStream("config.properties");
            prop.store(out, null);
        } catch (IOException e) {
            e.printStackTrace();
        }
        this.ip=ip;
        this.user=user;
        this.password=pass;
        this.adminPassword=adminPass;
    }

    //comprobaciones llamando a funciones de sql
    public boolean relojCodigoBarrasYaExiste(String isbn) {
        String consulta="SELECT existeCodigoBarras(?)";
        PreparedStatement function;
        boolean codigoBarrasExists=false;
        try {
            function=conexion.prepareStatement(consulta);
            function.setString(1,isbn);
            ResultSet rs =function.executeQuery();
            rs.next();
            codigoBarrasExists=rs.getBoolean(1);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return codigoBarrasExists;
    }

    public boolean fabricanteNombreYaExiste(String nombre) {
        String fabricanteNameConsult = "SELECT existeNombreFabricante(?)";
        PreparedStatement function;
        boolean nameExists = false;
        try {
            function = conexion.prepareStatement(fabricanteNameConsult);
            function.setString(1, nombre);
            ResultSet rs = function.executeQuery();
            rs.next();

            nameExists = rs.getBoolean(1);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return nameExists;
    }
    public boolean compradorNombreYaExiste(String nombre, String apellidos) {
        String completeName = apellidos + ", " + nombre;
        String compradorNameConsult = "SELECT existeNombreComprador(?)";
        PreparedStatement function;
        boolean nameExists = false;
        try {
            function = conexion.prepareStatement(compradorNameConsult);
            function.setString(1, completeName);
            ResultSet rs = function.executeQuery();
            rs.next();

            nameExists = rs.getBoolean(1);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return nameExists;
    }


}
