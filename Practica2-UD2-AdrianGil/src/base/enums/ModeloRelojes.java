package base.enums;

/**
 * Esta clase enum enumera las constantes con las que se rellena
 * el JComoboBox comboGenero de la vista.
 * Representan los géneros literarios que existen.
 */
public enum ModeloRelojes {
    RELOJDEPULSERA("Reloj de pulsera"),
    CRONOGRAFO("Cronografo"),
    CRONOMETRO("Cronometro"),
    RELOJDEBOLSILLO("Reloj de bolsillo"),
    RELOJDESALON("Reloj de salon"),
    RELOJDESOL("Reloj de sol");


    private String valor;

    ModeloRelojes(String valor){
        this.valor=valor;
    }

    public String getValor(){
        return valor;
    }
}

