package com.adriang.relojes.gui;

import com.adriang.relojes.base.Elegante;
import com.adriang.relojes.base.Reloj;
import com.adriang.relojes.base.Sport;
import com.adriang.relojes.util.Util;
import org.xml.sax.SAXException;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.*;
import java.util.Properties;

public class RelojControlador implements ActionListener, ListSelectionListener, WindowListener {
    private Ventana vista;
    private RelojModelo modelo;
    private File ultimaRutaExportada;
    public RelojControlador(Ventana vista, RelojModelo modelo){
        this.vista=vista;
        this.modelo=modelo;
        try {
            cargarDatosConfiguracion();
        } catch (IOException e) {
            System.out.println("No existe el fichero de configuracion"+ e.getMessage());
        }
        //añadimos los listener para que los botones funcionen
        addActionListener(this);
        addListSelectionListener(this);
        addWindowListener(this);
    }

    //cargamos los datos de la configuracion
    private void cargarDatosConfiguracion() throws IOException {
        Properties configuracion = new Properties();
        configuracion.load(new FileReader("relojes.conf"));
        ultimaRutaExportada =new File(configuracion.getProperty("ultimaRuta"));
    }
    //actualizamos datos
    private void actualizarDatosConfiguracion(File ultimaRutaExportada){
        this.ultimaRutaExportada =ultimaRutaExportada;
    }
    //y guardamos datos
    private void guardarDatosConfiguracion() throws IOException{
        Properties configuracion=new Properties();
        configuracion.setProperty("ultimaRutaExportada", ultimaRutaExportada.getAbsolutePath());
        configuracion.store(new PrintWriter("reloj.conf"), "Datos configuracion relojes");

    }

    @Override
    //creamos un switch con el action event para la eleccion de botones y su ejecucion
    public void actionPerformed(ActionEvent e) {
        String actionCommand=e.getActionCommand();

        switch(actionCommand){
            case "Nuevo":
                if(hayCamposVacios()){
                    Util.mensajeError(("Los siguientes campos no pueden estar vacios"+
                            "Marca \n Correa \n Material \n Fecha fabricacion\n" + vista.nEsferasAgujeros.getText()));
                    break;
                }
                if(modelo.existeMarca(vista.marcaTxt.getText())){
                    Util.mensajeError("Ya existe un reloj con esta marca");
                    vista.nEsferasAgujeros.getText();
                    break;

                }
                if(vista.sportRadioButton.isSelected()){
                    modelo.altaSport(vista.marcaTxt.getText(),
                            vista.correaTxt.getText(),
                            vista.materialTxt.getText(),
                            vista.fechaFabricacionDPiker.getDate(),
                            Integer.parseInt(vista.nEsferasAgujeros.getText()));

            }else{
                    modelo.altaElegante(vista.marcaTxt.getText(),
                            vista.correaTxt.getText(),
                            vista.materialTxt.getText(),
                            vista.fechaFabricacionDPiker.getDate(),
                            Integer.parseInt(vista.nEsferasAgujeros.getText()));
        }
                limpiarCampos();
                refrescar();
                break;
            case "Importar":
                JFileChooser selectorFichero= Util.crearSelectorFichero(ultimaRutaExportada, "Archivo XML", "xml");
                int opt=selectorFichero.showOpenDialog(null);
                if(opt==JFileChooser.APPROVE_OPTION){

                    try {
                        modelo.importarXML((selectorFichero.getSelectedFile()));
                    } catch (IOException ex) {
                        ex.printStackTrace();
                    } catch (SAXException ex) {
                        ex.printStackTrace();
                    } catch (ParserConfigurationException ex) {
                        ex.printStackTrace();
                    }
                    refrescar();

                }
                break;
            case "Exportar":
                JFileChooser selectorFichero2=Util.crearSelectorFichero(ultimaRutaExportada, "Archivo XML", "xml" );
                int opt2=selectorFichero2.showSaveDialog(null);
                if(opt2==JFileChooser.APPROVE_OPTION){
                    try {
                        modelo.exportarXML(selectorFichero2.getSelectedFile());
                        actualizarDatosConfiguracion(selectorFichero2.getSelectedFile());
                    } catch (ParserConfigurationException ex) {
                        ex.printStackTrace();
                    } catch (TransformerException ex) {
                        ex.printStackTrace();
                    }
                }
                break;
            case "Elegante":
                vista.EsferaAgujeroslbl.setText("N Esferas");
                break;
            case "Sport":
                vista.EsferaAgujeroslbl.setText("N Agujeros");
                break;
        }

    }

    @Override
    public void valueChanged(ListSelectionEvent e) {
        //solo ejecutamos el codigo si el valor se esta ajustando
        if(e.getValueIsAdjusting()) {
            Reloj relojSeleccionado = (Reloj) vista.list1.getSelectedValue();
            vista.marcaTxt.setText(relojSeleccionado.getMarca());
            vista.correaTxt.setText(relojSeleccionado.getCorrea());
            vista.materialTxt.setText(relojSeleccionado.getMaterial());
            vista.fechaFabricacionDPiker.setDate(relojSeleccionado.getFechaFabricacion());

            if(relojSeleccionado instanceof Sport){
                vista.sportRadioButton.doClick();
                vista.nEsferasAgujeros.setText(String.valueOf(((Sport) relojSeleccionado).getNumeroAgujeros()));
            }else{
                vista.eleganteRadioButton.doClick();
                vista.nEsferasAgujeros.setText(String.valueOf(((Elegante) relojSeleccionado).getCantidadEsferas()));
            }
        }

    }

    private void addActionListener(ActionListener listener){
        //listener para todos los botones
        vista.sportRadioButton.addActionListener(listener);
        vista.eleganteRadioButton.addActionListener(listener);
        vista.exportarButton.addActionListener(listener);
        vista.importarButton.addActionListener(listener);
        vista.nuevoButton.addActionListener(listener);
    }

    //listener del frame
    private void addWindowListener(WindowListener listener){
        vista.frame.addWindowListener(listener);
    }

    //listener de la lista
    private void addListSelectionListener(ListSelectionListener listener){
        vista.list1.addListSelectionListener(listener);
    }

    //limpieza de los campos
    private void limpiarCampos()  {
        vista.nEsferasAgujeros.setText(null);
        vista.marcaTxt.setText(null);
        vista.correaTxt.setText(null);
        vista.materialTxt.setText(null);
        vista.fechaFabricacionDPiker.setText(null);
        vista.marcaTxt.requestFocus();
    }

    //control por si hay campos vacios
    private boolean hayCamposVacios() {
        if (vista.nEsferasAgujeros.getText().isEmpty() || vista.marcaTxt.getText().isEmpty() || vista.correaTxt.getText().isEmpty() || vista.materialTxt.getText().isEmpty() || vista.fechaFabricacionDPiker.getText().isEmpty()) {
            return true;
        }


        return false;
    }

    //cargamos los datos de la lista de relojes
    private void refrescar() {
        vista.dlmReloj.clear();
        for (Reloj unReloj : modelo.obtenerRelojes()) {
            vista.dlmReloj.addElement(unReloj);
        }
    }

    @Override
    //metodo para la ventana emergente cuando vamos a cerrar le programa
    public void windowClosing(WindowEvent e) {
        int resp = Util.mensajeConfirmacion("¿Desea cerrar la ventana?", "Salir");
        if (resp == JOptionPane.NO_OPTION) {
            try {
                guardarDatosConfiguracion();
            } catch (IOException e1) {
                e1.printStackTrace();
            }
            System.exit(0);
        }
    }
    public void windowOpened(WindowEvent e) {

    }

    @Override
    public void windowClosed(WindowEvent e) {

    }

    @Override
    public void windowIconified(WindowEvent e) {

    }

    @Override
    public void windowDeiconified(WindowEvent e) {

    }

    @Override
    public void windowActivated(WindowEvent e) {

    }

    @Override
    public void windowDeactivated(WindowEvent e) {

    }


}
