CREATE DATABASE if not exists relojeria;
--
USE relojeria;
--
create table if not exists compradores(
idcomprador int auto_increment primary key,
nombre varchar(50) not null,
apellidos varchar(150) not null,
fechanacimiento date,
pais varchar(50));
--
create table if not exists fabricantes (
idfabricante int auto_increment primary key,
editorial varchar(50) not null,
email varchar(100) not null,
telefono varchar(9),
tipofabricante varchar(50),
web varchar(500));
--
create table if not exists relojes(
idreloj int auto_increment primary key,
marca varchar(50) not null,
codigoBarras varchar(40) not null UNIQUE,
idfabricante int not null,
modelo varchar(30) not null,
idcomprador int not null,
precio float not null,
fechalanzamiento date);
--
alter table relojes
	add foreign key (idcomprador) references fabricantes(idfabricante),
    add foreign key (idcomprador) references compradores(idcomprador);
--
delimiter ||
create function existecodigoBarras(f_codigoBarras varchar(40))
returns bit
begin
	declare i int;
    set i = 0;
    while ( i < (select max(idreloj) from relojes)) do
    if  ((select codigoBarras from relojes where idreloj = (i + 1)) like f_codigoBarras) then return 1;
    end if;
    set i = i + 1;
    end while;
    return 0;
end; ||
delimiter ;
--
delimiter ||
create function existeNombreFabricante(f_name varchar(50))
returns bit
begin
	declare i int;
    set i = 0;
    while ( i < (select max(idfabricante) from fabricantes)) do
    if  ((select fabricante from fabricantes where idfabricante = (i + 1)) like f_name) then return 1;
    end if;
    set i = i + 1;
    end while;
    return 0;
end; ||
delimiter ;
--
delimiter ||
create function existeNombreComprador(f_name varchar(202))
returns bit
begin
	declare i int;
    set i = 0;
    while ( i < (select max(idcomprador) from compradores)) do
    if  ((select concat(apellidos, ', ', nombre) from compradores where idcomprador = (i + 1)) like f_name) then return 1;
    end if;
    set i = i + 1;
    end while;
    return 0;
end; ||
delimiter ;
