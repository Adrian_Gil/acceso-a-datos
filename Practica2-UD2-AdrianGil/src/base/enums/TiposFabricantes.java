package base.enums;

/**
 * Esta clase enum enumera las constantes con las que se rellena
 * el JComoboBox comboTipoEditorial de la vista.
 * Representan los de eeditorial que existen que existen.
 */
public enum TiposFabricantes {
    ROLEX("Rolex"),
    OMEGA("Omega"),
    BREITLING("Breitling"),
    PATEKPHILIPPE("Patek Philippe"),
    AUDEMARSPIGUET("Audemars Piguet"),
    CARTIER("Cartier"),
    TUDOR("Tudor"),
    RICHARDMILLE("Richard Mille"),
    HMOSERANDCIE("H. Moser & Cie"),
    FPJOURNE("F.P. Journe");

    private String valor;

    TiposFabricantes(String valor){
        this.valor=valor;
    }

    public String getValor(){
        return valor;
    }
}
