package com.adriang.relojes.gui;

import com.adriang.relojes.base.Reloj;
import com.github.lgooddatepicker.components.DatePicker;

import javax.swing.*;

public class Ventana {
    private JPanel panel1;
    public JButton nuevoButton;
    public JButton importarButton;
    public JButton exportarButton;
    public JRadioButton sportRadioButton;
    public JTextField marcaTxt;
    public JRadioButton eleganteRadioButton;
    public JTextField correaTxt;
    public JTextField materialTxt;
    public JList list1;
    public JTextField nEsferasAgujeros;
    public DatePicker fechaFabricacionDPiker;
    public JLabel EsferaAgujeroslbl;

    public JFrame frame;

    public DefaultListModel<Reloj> dlmReloj;

    public Ventana(){
        frame=new JFrame("Reloj");
        frame.setContentPane(panel1);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
        frame.setLocationRelativeTo(null);

        initComponents();
    }

    private void initComponents() {
        dlmReloj=new DefaultListModel<Reloj>();
        list1.setModel(dlmReloj);
    }
}
