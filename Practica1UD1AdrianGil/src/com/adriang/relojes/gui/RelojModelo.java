package com.adriang.relojes.gui;

import com.adriang.relojes.base.Elegante;
import com.adriang.relojes.base.Reloj;
import com.adriang.relojes.base.Sport;
import org.w3c.dom.*;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;
import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;

public class RelojModelo {
    private ArrayList<Reloj> listaRelojes;

    //constructor de RelojModelo con el ArrayList llamando a la clase reloj
    public RelojModelo() {
        listaRelojes = new ArrayList<Reloj>();
    }

    public ArrayList<Reloj> obtenerRelojes() {
        return listaRelojes;
    }
    //rellenamos los datos de Sport o Elegante y los añadimos a nuevoSport o nuevoElegante
    public void altaSport(String marca, String correa, String material, LocalDate fechaFabricacion, int numeroAgujeros) {
        Sport nuevoSport = new Sport(marca, correa, material, fechaFabricacion, numeroAgujeros);
        listaRelojes.add(nuevoSport);
    }

    public void altaElegante(String marca, String correa, String material, LocalDate fechaFabricacion, int numeroEsferas) {
        Elegante nuevoElegante = new Elegante(marca, correa, material, fechaFabricacion, numeroEsferas);
        listaRelojes.add(nuevoElegante);
    }

    public boolean existeMarca(String marca) {
        for (Reloj unReloj : listaRelojes) {
            if (unReloj.getMarca().equals(marca)) {
                return true;
            }
        }
        return false;
    }

    public void exportarXML(File fichero) throws ParserConfigurationException, TransformerException {
        //creamos un xml vacio mediante el metodo DocumentBuilderFactory
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        DOMImplementation dom = builder.getDOMImplementation();
        Document documento = dom.createDocument(null, "xml", null);

        //creamos el elmento raiz llamado Relojes
        Element raiz = documento.createElement("Relojes");
        documento.getDocumentElement().appendChild(raiz);

        Element nodoReloj = null;
        Element nodoDatos = null;
        Text texto = null;

        //hacemos un foreach dependiendo lo que eligamos entre reloj de Sport o reloj Elegante
        for (Reloj unReloj : listaRelojes) {
            if (unReloj instanceof Sport) {
                nodoReloj = documento.createElement("Sport");
            } else {
                nodoReloj = documento.createElement("Elegante");
            }
            raiz.appendChild(nodoReloj);

            //rellenamos cada uno de los nodosDatos con los distinitos valores que tenemos para los relojes
            nodoDatos = documento.createElement("marca");
            nodoReloj.appendChild(nodoDatos);
            texto = documento.createTextNode(unReloj.getMarca());
            nodoDatos.appendChild(texto);

            nodoDatos = documento.createElement("correa");
            nodoReloj.appendChild(nodoDatos);
            texto = documento.createTextNode(unReloj.getCorrea());
            nodoDatos.appendChild(texto);

            nodoDatos = documento.createElement("material");
            nodoReloj.appendChild(nodoDatos);
            texto = documento.createTextNode(unReloj.getMaterial());
            nodoDatos.appendChild(texto);

            nodoDatos = documento.createElement("fecha-fabricacion");
            nodoReloj.appendChild(nodoDatos);
            texto = documento.createTextNode(unReloj.getFechaFabricacion().toString());
            nodoDatos.appendChild(texto);

            //realizamos un instanceof ya que dependiendo de lo que eligan si sport o elegante podran decir el numero de agujeros que tiene la correa o el numero de esferas que tiene el reloj
            if (unReloj instanceof Sport) {
                nodoDatos = documento.createElement("numero-agujeros");
                nodoReloj.appendChild(nodoDatos);
                texto = documento.createTextNode(String.valueOf(((Sport) unReloj).getNumeroAgujeros()));
                nodoDatos.appendChild(texto);
            } else {
                nodoDatos = documento.createElement("numero-esferas");
                nodoReloj.appendChild(nodoDatos);
                texto = documento.createTextNode(String.valueOf(((Elegante) unReloj).getCantidadEsferas()));
                nodoDatos.appendChild(texto);
            }

            //guardamos los datos en un fichero
            Source source = new DOMSource(documento);
            Result resultado = new StreamResult(fichero);

            Transformer transformer = TransformerFactory.newInstance().newTransformer();
            transformer.transform(source, resultado);
        }
    }

    public void importarXML(File fichero) throws IOException, SAXException, ParserConfigurationException {
        listaRelojes = new ArrayList<Reloj>();
        Sport nuevoSport = null;
        Elegante nuevoElegante = null;

        //creamos el xml y cargarmos con los datos del fichero el cual anteriormente habiamos exportado
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        Document documento = builder.parse(fichero);

        NodeList listaElementos = documento.getElementsByTagName("*");

        for (int i = 0; i < listaElementos.getLength(); i++) {
            Element nodoReloj = (Element) listaElementos.item(i);

            //dependiendo de si elegimos Sport o Elegante cargaremos unos datos o otros
            if (nodoReloj.getTagName().equals("Sport")) {
                nuevoSport = new Sport();
                nuevoSport.setMarca(nodoReloj.getChildNodes().item(0).getTextContent());
                nuevoSport.setMaterial(nodoReloj.getChildNodes().item(1).getTextContent());
                nuevoSport.setCorrea(nodoReloj.getChildNodes().item(2).getTextContent());
                nuevoSport.setFechaFabricacion(LocalDate.parse(nodoReloj.getChildNodes().item(3).getTextContent()));
                nuevoSport.setNumeroAgujeros(Integer.parseInt(nodoReloj.getChildNodes().item(4).getTextContent()));
                listaRelojes.add(nuevoSport);

            } else {
                if (nodoReloj.getTagName().equals("Moto")) {
                    nuevoElegante = new Elegante();
                    nuevoElegante.setMarca(nodoReloj.getChildNodes().item(0).getTextContent());
                    nuevoElegante.setMaterial(nodoReloj.getChildNodes().item(1).getTextContent());
                    nuevoElegante.setCorrea(nodoReloj.getChildNodes().item(2).getTextContent());
                    nuevoElegante.setFechaFabricacion(LocalDate.parse(nodoReloj.getChildNodes().item(3).getTextContent()));
                    nuevoElegante.setCantidadEsferas(Integer.parseInt(nodoReloj.getChildNodes().item(4).getTextContent()));
                    listaRelojes.add(nuevoElegante);
                }
            }
        }

    }

}