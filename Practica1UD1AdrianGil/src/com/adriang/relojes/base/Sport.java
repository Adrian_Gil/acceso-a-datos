package com.adriang.relojes.base;

import java.time.LocalDate;

public class Sport extends Reloj{
    private int numeroAgujeros;

    public Sport(){
        super();
    }

    public Sport(String marca, String correa, String material, LocalDate fechaFabricacion, int numeroAgujeros) {
        super(marca, correa, material, fechaFabricacion);
        this.numeroAgujeros = this.numeroAgujeros;
    }

    public int getNumeroAgujeros(){ return numeroAgujeros; }

    public void setNumeroAgujeros(int numeroAgujeros){
        this.numeroAgujeros=numeroAgujeros;
    }

    @Override
    public String toString() {
        return "Reloj{"
                +"marca=" +getMarca()
                +"correa=" +getCorrea()
                +"material=" +getMaterial()
                + '}';
    }
}
